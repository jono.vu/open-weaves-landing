const path = require(`path`)

module.exports = {
  pathPrefix: "/",
  siteMetadata: {
    siteUrl: "https://www.openweaves.com/",
    pathPrefix: "/",
    title: `Open Weaves - Weaving Soon`,
    titleAlt: `Open Weaves`,
    description: `Soft and practical tailoring, handmade to your needs.`,
    banner: "./static/hero.png",
    headline: "",
    siteLanguage: "en",
    ogLanguage: "en_US",
    author: "Open Weaves",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Open Weaves - Weaving Soon`,
        short_name: `Open Weaves`,
        description: `Soft and practical tailoring, handmade to your needs.`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#111`,
        display: `standalone`,
        icon: `./static/favicon.png`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`),
      },
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-179156994-1",
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        // head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        // exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Delays sending pageview hits on route update (in milliseconds)
        // pageTransitionDelay: 0,
        // Enables Google Optimize using your container Id
        // optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
        // Enables Google Optimize Experiment ID
        // experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
        // Set Variation ID. 0 for original 1,2,3....
        // variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
        // Defers execution of google analytics script after page load
        defer: true,
        // Any additional optional fields
        // sampleRate: 5,
        // siteSpeedSampleRate: 10,
        // cookieDomain: "openweaves.com",
      },
    },
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-emotion`,
    },
  ],
}
