import React, { useState } from "react"
import { useForm } from "react-hook-form"
import { useMediaQuery } from "react-responsive"
import { Responsive } from "./Responsive"

const SignupForm = () => {
  const [isTouched, setTouched] = useState<boolean>(false)
  const [isCompleted, setCompleted] = useState<boolean>(false)

  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
    errors,
    setError,
  } = useForm<{ email: string }>()

  const onSubmit = async ({ email }) => {
    if (email) {
      const res = await fetch("/", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: encode({ "form-name": "contact", ...{ email } }),
      })

      if (res.ok) setCompleted(true)
      else {
        setError("email", {
          message: `${res.status}: Something went wrong. Please contact j@openweaves.com directly!`,
        })
      }
    }
  }

  const Tag = () => {
    if (errors["email"])
      return (
        <a
          target="_blank"
          className="hover cursor-pointer"
          href="mailto:j@openweaves.com"
        >
          {errors["email"].message}
        </a>
      )
    const sm = <span className="hover">Subscribe</span>
    const md = (
      <span className="hover">Subscribe for Open Weaves news and updates</span>
    )
    return <Responsive {...{ sm, md }} />
  }

  if (isCompleted) return <span>Thank you!</span>

  if (!isTouched)
    return (
      <span onClick={() => setTouched(true)} className="hover cursor-pointer">
        Subscribe
      </span>
    )

  return (
    <div>
      <Tag />
      <form
        onSubmit={handleSubmit(onSubmit)}
        name="contact"
        method="POST"
        data-netlify="true"
      >
        <label htmlFor="email" hidden>
          Email
        </label>
        <input
          id="email"
          type="email"
          name="email"
          disabled={isSubmitting}
          placeholder="e-mail"
          ref={register({ required: true, minLength: 1 })}
          style={{ marginRight: 8 }}
        />
        <button type="submit" disabled={isSubmitting}>
          go
        </button>
      </form>
    </div>
  )
}

export { SignupForm }

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&")
}
