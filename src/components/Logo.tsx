import styled from "@emotion/styled"
import React, { useState } from "react"

import { useMediaQuery } from "react-responsive"

import logo from "../images/logo.svg"
import { Responsive } from "./Responsive"

const Logo = () => {
  const [isHovered, setHovered] = useState<boolean>(false)

  const sm = (
    <MobileContainer>
      <img src={logo} alt="logo" style={{ width: "80vw" }} />
      <span>Weaving soon</span>
    </MobileContainer>
  )

  const md = (
    <HoverContainer
      onMouseOver={() => setHovered(true)}
      onMouseOut={() => setHovered(false)}
    >
      {!isHovered ? (
        <img
          src={logo}
          alt="logo"
          width={500}
          style={{ pointerEvents: "none" }}
        />
      ) : (
        <span style={{ pointerEvents: "none" }}>Weaving soon</span>
      )}
    </HoverContainer>
  )

  const lg = (
    <HoverContainer
      onMouseOver={() => setHovered(true)}
      onMouseOut={() => setHovered(false)}
    >
      {!isHovered ? (
        <img
          src={logo}
          alt="logo"
          width={700}
          style={{ pointerEvents: "none" }}
        />
      ) : (
        <span style={{ pointerEvents: "none" }}>Weaving soon</span>
      )}
    </HoverContainer>
  )

  return <Responsive {...{ sm, md, lg }} />
}

export { Logo }

const MobileContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  flex-direction: column;
`

const HoverContainer = styled.div`
  width: 700px;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`
