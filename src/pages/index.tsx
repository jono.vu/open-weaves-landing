import styled from "@emotion/styled"
import { graphql } from "gatsby"
import BackgroundImage from "gatsby-background-image"
import "normalize.css"
import React from "react"
import { Logo } from "../components/Logo"
import Metadata from "../components/Metadata"
import { SignupForm } from "../components/SignupForm"
import "./main.css"

const Home = ({ data }) => {
  const bgImage = data.file.childImageSharp.fluid

  return (
    <Main>
      <Metadata />
      <BackgroundImage
        fluid={bgImage}
        style={{
          height: "100%",
          width: "100%",
          position: "absolute",
          backgroundPosition: "34% 50%",
        }}
        preserveStackingContext
      />

      <Box>
        <Logo />
      </Box>
      <div />
      <Footer>
        <SignupForm />
        <a
          target="_blank"
          href="https://www.instagram.com/openweaves"
          className="hover cursor-pointer"
        >
          Instagram
        </a>
      </Footer>
    </Main>
  )
}

export default Home

const Main = styled.div`
  height: 100vh;
  width: 100vw;
  top: 0;
  left: 0;

  display: grid;
  grid-template-rows: 1fr auto;
`

const Box = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
`

const Footer = styled.footer`
  position: relative;
  padding: 32px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  @media (max-width: 1224px) {
    position: absolute;
    bottom: 0;
    width: 100%;
    box-sizing: border-box;
    padding: 16px;
  }
`

export const query = graphql`
  query {
    file(relativePath: { eq: "bg.jpg" }) {
      childImageSharp {
        fluid(quality: 90, base64Width: 1920) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
